# ES6 UI project 

Display the list of properties using the data set within the "results " array running down the page in a column that has a heading labeled Results.
There will be a second column with a heading Saved Properties, along side the Results column. It will contain the initial property within the
"saved" array
Hovering over a property card in the Results column will display an ’add’ button. Clicking the ‘add’ button will create the property in the Saved
Properties column. Hovering over a property card in the Saved Properties column will display a 'remove’ button. Clicking the ‘remove' button
will remove the property from the list of saved properties.

## Prerequisites

Latest [Node.js](https://nodejs.org/en/)  and [NPM](https://npmjs.org) installed. 
The project is built on node v8.3.0 and npm v5.3.0.    
Use node -v and npm -v to check the versions



## Getting Started

### Project setup

The project is complied using webpack, dev-server and babel. You will have to install it before you start

1. Create a new folder
2. Clone the git repository (git clone https://dspanditha@bitbucket.org/dspanditha/es6-ui-project.git) to the above folder 
3. go into the root folder of the project


Run the following commands to install dependancies

4. `npm install -g webpack`
5. `npm install babel-core babel-loader`
6. `npm install babel-preset-es2015`
7. `npm install -g webpack-dev-server`


### Project build
 
Run the following command to compile and build the project
 `npm run build`


Then run the the following command to start up the server
 `npm run server-start`

Then load the project in browser with  http://localhost:"portname shown in commandline"/src/

 **Eg: http://localhost:8080/src/**



## Running the tests

### Test setup

Run the following command to install dependancies for testing

`npm install -g mocha` 

### Run tests

Run the following command to run the tests
`npm run test`


## Built With

* [Node.js](https://nodejs.org/en/)  and [NPM](https://npmjs.org) - JavaScript run-time environment
* [Babel](https://babeljs.io/) - Javascript compiler
* [Webpack](https://webpack.github.io/) - Module bundler
* [Mocha](https://mochajs.org/) - Testing framework



## Authors

Don Panditha: https://bitbucket.org/dspanditha/ 

## License

This project is licensed under the ISC License

