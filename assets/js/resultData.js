import result from '../../assets/files/data.json'

class resultData {

    constructor() {
        this.resultarr = result.results;
        this.results = result.results;
    }


    /**
     * return result json object
     * @return  array object
     */
    getResultArray() {

        return result.results;

    }


    /**
    * search through result array
    * @param {*} id 
    * @return arrayobj
    */
    searchResultArray(id) {

        var arrayobj = this.results.find(x => x.id == id);

        return arrayobj
    }


    /**
     * display array in DOM elements 
     * @param {*} disarray 
     */
    displayElements(disarray) {

        let html = '';
        let agency = [];

        if (disarray.length > 0) {

            disarray.forEach(function (val, index) {
                html += `<div class="card-block card-custom container m-t-10 m-b-10"><table class="col-md-12 m-b-10 format-padding text-left">
                <tr><td style=" background-color:${val.agency.brandingColors.primary} "class='header'> <img src='${val.agency.logo}'></td></tr>
                <tr> <td class="format-padding"> <img class='img-thumbnail no-border format-padding' src='${val.mainImage}'></td></tr>
                <tr><td ><h4 class="m-b-0">${val.price}</h4></td></tr></table>
                <div class="overlay">
                <button class='add-btn btn btn-success btn-lg'  id='${val.id}' ><i class='fa fa-plus'></i> Add Property</button>
                </div>
                </div>`;

                document.getElementById("result").innerHTML = html;

            });
        } else {

            document.getElementById("result").innerHTML = `<h5 class="text-danger w-300"> NO results to be displayed</h5>`;
        }


    }


    /**
     * add property from array
     * @param {*} btnobj 
     * @return Boolean
     */
    addProperty(btnobj) {

        var initLength = this.results.length;
        var newLength = this.results.push(btnobj);

        return (newLength > initLength);

    }


    /**
     * remove property from array
     * @param {*} btnId 
     */
    removeProperty(btnId) {

        var arr = this.results
        var index = arr.findIndex(x => x.id == btnId);

        var removed = arr.splice(index, 1);

        return removed.length == 1;

    }


};

export default resultData;
