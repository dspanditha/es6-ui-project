import result from '../../assets/files/data.json'

class savedData {

    constructor() {
        this.savedarr = result.saved;
        this.saved = this.savedarr;
    }


    /**
     * return saved json object
     * @return saved object
     */
    getSavedArray() {

        return this.savedarr
    }


    /**
     * search through result array
     * @param {*} id 
     * @return array
     */
    searchSavedArray(id) {
        var arrayobj = this.saved.find(x => x.id == id);

        return arrayobj
    }


    /**
     * display array in DOM elements
     * @param {*} disarray 
     */
    displayElements(disarray) {

        let htmls = '';

        if (disarray.length > 0) {

            disarray.forEach(function (val, index) {
                htmls += `<div class="card-block card-custom container m-t-10 m-b-10"><table class="col-md-12 m-b-10 format-padding text-left">
                <tr><td style=" background-color:${val.agency.brandingColors.primary} "class='header '> <img src='${val.agency.logo}'></td></tr>
                <tr> <td class="format-padding"> <img class='img-thumbnail no-border format-padding' src='${val.mainImage}'></td></tr>
                <tr><td><h4 class="m-b-0">${val.price}</h4></td></tr></table>
                <div class="overlay">
                <button class='save-btn btn btn-danger  btn-lg' id='${val.id}' > <i class='fa fa-times'></i> Remove Property</button>
                </div>
                </div>`;

                document.getElementById("saved").innerHTML = htmls;

            });
        } else {

            document.getElementById("saved").innerHTML = `<h5 class="text-danger w-300"> NO saved properties to be displayed</h5>`;
        }

    }


    /**
     * add property from array
     * @param {*} btnobj 
     */
    addProperty(btnobj) {

        var initLength = this.saved.length;
        var newLength = this.saved.push(btnobj)

        return (newLength > initLength);

    }


    /**
     * remove property from array
     * @param {*} btnId 
     */
    removeProperty(btnId) {

        var arr = this.saved
        var index = arr.findIndex(x => x.id == btnId);

        var removed = arr.splice(index, 1);

        return removed.length == 1;

    }


};

export default savedData;
