import resultData from './resultData.js'
import savedData from './savedData.js'


let resultdata = new resultData();
let saveddata = new savedData();

var remove_mod_element = document.querySelector("#static-modal-remove");
var remove_mod_closebtn = document.getElementById("remove_close");
var remove_mod_conformbtn = document.getElementById("remove_confirm");

var save_mod_element = document.querySelector("#static-modal-save");
var save_mod_closebtn = document.getElementById("save_close");
var save_mod_conformbtn = document.getElementById("save_confirm");

resultdata.displayElements(resultdata.results);
saveddata.displayElements(saveddata.saved);

addBtnClickEvent();
removeBtnClickEvent();


/**
 * add click events to add button
 */
function addBtnClickEvent() {

    var resBtn = document.querySelectorAll(".add-btn");

    Array.from(resBtn).forEach(function (button) {
        button.addEventListener('click', function (event) {

            save_mod_element.style.display = "block";

            save_mod_closebtn.onclick = function () {
                save_mod_element.style.display = "none";

            }
            save_mod_conformbtn.onclick = function () {
                save_mod_element.style.display = "none";
                var obj = resultdata.searchResultArray(button.id);
                var removereturn = resultdata.removeProperty(button.id);
                resultdata.displayElements(resultdata.results);

                if (saveddata.addProperty(obj)) {
                    saveddata.displayElements(saveddata.saved.reverse());
                } else {
                    alert("property cannot be added");
                }

                addBtnClickEvent();
                removeBtnClickEvent();

            }

        });
    });
}



/**
 * add click events to remove button
 */
function removeBtnClickEvent() {


    var saveBtn = document.querySelectorAll(".save-btn");

    Array.from(saveBtn).forEach(button => {
        button.addEventListener('click', function (event) {

            remove_mod_element.style.display = "block";

            remove_mod_closebtn.onclick = function () {
                remove_mod_element.style.display = "none";

            }

            remove_mod_conformbtn.onclick = function () {
                remove_mod_element.style.display = "none";
                var obj = saveddata.searchSavedArray(button.id);
                var removereturn = saveddata.removeProperty(button.id);
                saveddata.displayElements(saveddata.saved);
                removeBtnClickEvent();
            }




        });
    });

}

