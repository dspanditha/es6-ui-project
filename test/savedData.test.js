import { expect, assert, should } from 'chai';
import savedData from '../assets/js/savedData.js';



describe("savedData  Module", function () {

    var saveddata = new savedData();
    var arrayobj = saveddata.getSavedArray();
    var obj = {
        price: '$726,500', agency: {
            brandingColors: { primary: '#ffe512' },
            logo: 'http://i1.au.reastatic.net/agencylogo/XRWXMT/12/20120927204448.gif'
        },
        id: '1',
        mainImage: 'http://i2.au.reastatic.net/640x480/20bfc8668a30e8cabf045a1cd54814a9042fc715a8be683ba196898333d68cec/main.jpg'
    }


    describe("Return an array object: getSavedArray ", function () {
        it("Returns valid array of json objects", function () {

            assert.typeOf(arrayobj, 'array');
        });

        it("array object has 3 items", function () {

            expect(arrayobj).to.have.length(1);
        });

    });


    describe("serach for an object in array : searchSavedArray ", function () {
        it("Returns an array object", function () {

            expect(saveddata.searchSavedArray("4")).to.be.a('object');
            // assert.typeOf(savedData.searchSavedArray("1"), 'object');
        });

    });


    describe("Add an object to Saved array : addProperty ", function () {

        it("Array object has 1 item", function () {

            expect(arrayobj).to.have.length(1);
        });

        it("addproperty returns true", function () {

            expect(saveddata.addProperty(obj)).to.be.true;
        });

        it("Array length increased by 1", function () {

            expect(arrayobj).to.have.length(2);
        });

    });


    describe("Remove an object to Saved array : removeProperty ", function () {


        it("Array object has 2 items", function () {

            expect(arrayobj).to.have.length(2);
        });

        it("Array length decreased by 1", function () {

            saveddata.removeProperty("1");
            expect(arrayobj).to.have.length(1);
        });

        it("removeproperty returns true", function () {

            expect(saveddata.removeProperty(obj)).to.be.true;
        });

    });


});


//test both  modules
    //json is null
    //getSaved
    //add property  
    //remove property
    //search Saved array
