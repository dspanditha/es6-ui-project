import { expect, assert, should } from 'chai';
import resultData from '../assets/js/resultData.js'



describe("resultData  Module", function () {

    var resultdata = new resultData();
    var arrayobj = resultdata.getResultArray();
    var obj = {
        price: '$726,500', agency: {
            brandingColors: { primary: '#ffe512' },
            logo: 'http://i1.au.reastatic.net/agencylogo/XRWXMT/12/20120927204448.gif'
        },
        id: '1',
        mainImage: 'http://i2.au.reastatic.net/640x480/20bfc8668a30e8cabf045a1cd54814a9042fc715a8be683ba196898333d68cec/main.jpg'
    }


    describe("Return an array object: getResultArray ", function () {
        it("Returns valid array of json objects", function () {

            assert.typeOf(arrayobj, 'array');
        });

        it("array object has 3 items", function () {

            expect(arrayobj).to.have.length(3);
        });

    });


    describe("serach for an object in array : searchResultArray ", function () {
        it("Returns an array object", function () {

            expect(resultdata.searchResultArray("1")).to.be.a('object');
            // assert.typeOf(resultdata.searchResultArray("1"), 'object');
        });

    });


    describe("Add an object to result array : addProperty ", function () {

        it("Array object has 3 items", function () {

            expect(arrayobj).to.have.length(3);
        });


        it("addproperty returns true", function () {

            expect(resultdata.addProperty(obj)).to.be.true;
        });

        it("Array length increased by 1", function () {

            expect(arrayobj).to.have.length(4);
        });



    });


    describe("Remove an object to result array : removeProperty ", function () {


        it("Array object has 4 items", function () {

            expect(arrayobj).to.have.length(4);
        });

        it("Array length decreased by 1", function () {

            resultdata.removeProperty("1");
            expect(arrayobj).to.have.length(3);
        });

        it("removeproperty returns true", function () {

            expect(resultdata.removeProperty(obj)).to.be.true;
        });

    });


});


//test both  modules
    //json is null
    //getresult
    //add property  
    //remove property
    //search result array
