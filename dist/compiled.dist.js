/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = {"results":[{"price":"$726,500","agency":{"brandingColors":{"primary":"#ffe512"},"logo":"http://i1.au.reastatic.net/agencylogo/XRWXMT/12/20120927204448.gif"},"id":"1","mainImage":"http://i2.au.reastatic.net/640x480/20bfc8668a30e8cabf045a1cd54814a9042fc715a8be683ba196898333d68cec/main.jpg"},{"price":"$560,520","agency":{"brandingColors":{"primary":"#fcfa3b"},"logo":"http://i4.au.reastatic.net/agencylogo/BFERIC/12/20150619122858.gif"},"id":"2","mainImage":"http://i1.au.reastatic.net/640x480/88586227f9176f602d5c19cf06261108dbb29f03e30d1c4ce9fc2b51fb1e4bd6/main.jpg"},{"price":"$826,500","agency":{"brandingColors":{"primary":"#57B5E0"},"logo":"http://i1.au.reastatic.net/agencylogo/XCEWIN/12/20150807093203.gif"},"id":"3","mainImage":"http://i4.au.reastatic.net/640x480/98cee1b2a3a64329921fc38f7e2926a78d41fcc683fc48fb8a8ef2999b14c027/main.jpg"}],"saved":[{"price":"$526,500","agency":{"brandingColors":{"primary":"#000000"},"logo":"http://i2.au.reastatic.net/agencylogo/WVYSSK/2/20140701084436.gif"},"id":"4","mainImage":"http://i2.au.reastatic.net/640x480/5e84d96722dda3ea2a084d6935677f64872d1d760562d530c3cabfcb7bcda9c2/main.jpg"}]}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _data = __webpack_require__(0);

var _data2 = _interopRequireDefault(_data);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var resultData = function () {
    function resultData() {
        _classCallCheck(this, resultData);

        this.resultarr = _data2.default.results;
        this.results = _data2.default.results;
    }

    /**
     * return result json object
     * @return  array object
     */


    _createClass(resultData, [{
        key: 'getResultArray',
        value: function getResultArray() {

            return _data2.default.results;
        }

        /**
        * search through result array
        * @param {*} id 
        * @return arrayobj
        */

    }, {
        key: 'searchResultArray',
        value: function searchResultArray(id) {

            var arrayobj = this.results.find(function (x) {
                return x.id == id;
            });

            return arrayobj;
        }

        /**
         * display array in DOM elements 
         * @param {*} disarray 
         */

    }, {
        key: 'displayElements',
        value: function displayElements(disarray) {

            var html = '';
            var agency = [];

            if (disarray.length > 0) {

                disarray.forEach(function (val, index) {
                    html += '<div class="card-block card-custom container m-t-10 m-b-10"><table class="col-md-12 m-b-10 format-padding text-left">\n                <tr><td style=" background-color:' + val.agency.brandingColors.primary + ' "class=\'header\'> <img src=\'' + val.agency.logo + '\'></td></tr>\n                <tr> <td class="format-padding"> <img class=\'img-thumbnail no-border format-padding\' src=\'' + val.mainImage + '\'></td></tr>\n                <tr><td ><h4 class="m-b-0">' + val.price + '</h4></td></tr></table>\n                <div class="overlay">\n                <button class=\'add-btn btn btn-success btn-lg\'  id=\'' + val.id + '\' ><i class=\'fa fa-plus\'></i> Add Property</button>\n                </div>\n                </div>';

                    document.getElementById("result").innerHTML = html;
                });
            } else {

                document.getElementById("result").innerHTML = '<h5 class="text-danger w-300"> NO results to be displayed</h5>';
            }
        }

        /**
         * add property from array
         * @param {*} btnobj 
         * @return Boolean
         */

    }, {
        key: 'addProperty',
        value: function addProperty(btnobj) {

            var initLength = this.results.length;
            var newLength = this.results.push(btnobj);

            return newLength > initLength;
        }

        /**
         * remove property from array
         * @param {*} btnId 
         */

    }, {
        key: 'removeProperty',
        value: function removeProperty(btnId) {

            var arr = this.results;
            var index = arr.findIndex(function (x) {
                return x.id == btnId;
            });

            var removed = arr.splice(index, 1);

            return removed.length == 1;
        }
    }]);

    return resultData;
}();

;

exports.default = resultData;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _data = __webpack_require__(0);

var _data2 = _interopRequireDefault(_data);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var savedData = function () {
    function savedData() {
        _classCallCheck(this, savedData);

        this.savedarr = _data2.default.saved;
        this.saved = this.savedarr;
    }

    /**
     * return saved json object
     * @return saved object
     */


    _createClass(savedData, [{
        key: 'getSavedArray',
        value: function getSavedArray() {

            return this.savedarr;
        }

        /**
         * search through result array
         * @param {*} id 
         * @return array
         */

    }, {
        key: 'searchSavedArray',
        value: function searchSavedArray(id) {
            var arrayobj = this.saved.find(function (x) {
                return x.id == id;
            });

            return arrayobj;
        }

        /**
         * display array in DOM elements
         * @param {*} disarray 
         */

    }, {
        key: 'displayElements',
        value: function displayElements(disarray) {

            var htmls = '';

            if (disarray.length > 0) {

                disarray.forEach(function (val, index) {
                    htmls += '<div class="card-block card-custom container m-t-10 m-b-10"><table class="col-md-12 m-b-10 format-padding text-left">\n                <tr><td style=" background-color:' + val.agency.brandingColors.primary + ' "class=\'header \'> <img src=\'' + val.agency.logo + '\'></td></tr>\n                <tr> <td class="format-padding"> <img class=\'img-thumbnail no-border format-padding\' src=\'' + val.mainImage + '\'></td></tr>\n                <tr><td><h4 class="m-b-0">' + val.price + '</h4></td></tr></table>\n                <div class="overlay">\n                <button class=\'save-btn btn btn-danger  btn-lg\' id=\'' + val.id + '\' > <i class=\'fa fa-times\'></i> Remove Property</button>\n                </div>\n                </div>';

                    document.getElementById("saved").innerHTML = htmls;
                });
            } else {

                document.getElementById("saved").innerHTML = '<h5 class="text-danger w-300"> NO saved properties to be displayed</h5>';
            }
        }

        /**
         * add property from array
         * @param {*} btnobj 
         */

    }, {
        key: 'addProperty',
        value: function addProperty(btnobj) {

            var initLength = this.saved.length;
            var newLength = this.saved.push(btnobj);

            return newLength > initLength;
        }

        /**
         * remove property from array
         * @param {*} btnId 
         */

    }, {
        key: 'removeProperty',
        value: function removeProperty(btnId) {

            var arr = this.saved;
            var index = arr.findIndex(function (x) {
                return x.id == btnId;
            });

            var removed = arr.splice(index, 1);

            return removed.length == 1;
        }
    }]);

    return savedData;
}();

;

exports.default = savedData;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _resultData = __webpack_require__(1);

var _resultData2 = _interopRequireDefault(_resultData);

var _savedData = __webpack_require__(2);

var _savedData2 = _interopRequireDefault(_savedData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var resultdata = new _resultData2.default();
var saveddata = new _savedData2.default();

var remove_mod_element = document.querySelector("#static-modal-remove");
var remove_mod_closebtn = document.getElementById("remove_close");
var remove_mod_conformbtn = document.getElementById("remove_confirm");

var save_mod_element = document.querySelector("#static-modal-save");
var save_mod_closebtn = document.getElementById("save_close");
var save_mod_conformbtn = document.getElementById("save_confirm");

resultdata.displayElements(resultdata.results);
saveddata.displayElements(saveddata.saved);

addBtnClickEvent();
removeBtnClickEvent();

/**
 * add click events to add button
 */
function addBtnClickEvent() {

    var resBtn = document.querySelectorAll(".add-btn");

    Array.from(resBtn).forEach(function (button) {
        button.addEventListener('click', function (event) {

            save_mod_element.style.display = "block";

            save_mod_closebtn.onclick = function () {
                save_mod_element.style.display = "none";
            };
            save_mod_conformbtn.onclick = function () {
                save_mod_element.style.display = "none";
                var obj = resultdata.searchResultArray(button.id);
                var removereturn = resultdata.removeProperty(button.id);
                resultdata.displayElements(resultdata.results);

                if (saveddata.addProperty(obj)) {
                    saveddata.displayElements(saveddata.saved.reverse());
                } else {
                    alert("property cannot be added");
                }

                addBtnClickEvent();
                removeBtnClickEvent();
            };
        });
    });
}

/**
 * add click events to remove button
 */
function removeBtnClickEvent() {

    var saveBtn = document.querySelectorAll(".save-btn");

    Array.from(saveBtn).forEach(function (button) {
        button.addEventListener('click', function (event) {

            remove_mod_element.style.display = "block";

            remove_mod_closebtn.onclick = function () {
                remove_mod_element.style.display = "none";
            };

            remove_mod_conformbtn.onclick = function () {
                remove_mod_element.style.display = "none";
                var obj = saveddata.searchSavedArray(button.id);
                var removereturn = saveddata.removeProperty(button.id);
                saveddata.displayElements(saveddata.saved);
                removeBtnClickEvent();
            };
        });
    });
}

/***/ })
/******/ ]);